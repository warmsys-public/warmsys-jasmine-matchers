declare module jasmine
{
    interface Matchers
    {
        toBeInstanceOf(classConstructor:Function):boolean;
        toBeArray():boolean;
        toBeNumber():boolean;
        toBeObject():boolean;
        toBeString():boolean;
        toBeInRange(min:number, max:number):boolean;
        toBePresent(): boolean;
        toBeHidden(): boolean;
        toContainAtleast(options:{[index:string]:any}): boolean;
        toHaveBeenCalledWithAtleast(options:{[index:string]:any}): boolean;
        toContainJQueryElement(element:JQuery): boolean;
        toBeJQueryElement(element:JQuery): boolean;
        toHaveBeenCalledXTimes(xtimes:number):boolean;
        toBeAJQueryPromise():boolean;
        toHaveOwnProperties(...args:string[]):boolean;
        toBeOneOf(array:any[]):boolean;
    }
}
