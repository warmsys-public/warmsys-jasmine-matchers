var getTextForJQueryElement = function($element)
{
    var text = 'null';
    var element;
    if(element = $element.get(0))
    {
        text = element.nodeName.toLowerCase();
        if(element.id)
        {
            text += "#"+element.id;
        }
        if(element.className)
        {
            text += "."+element.className.replace(/\s+/g, ".");
        }
    }
    return text;
};

var objectContains = function(expected, actual, identifier)
{
    var result = {pass: false};
    var mismatchedExpects = {};
    _.each(expected, function(value, key)
    {
        if(!_.isEqual(actual[key], value))
        {
            mismatchedExpects[key] = actual[key] === undefined ? "undefined" : actual[key];
        }
    });

    result.pass = _.isEmpty(mismatchedExpects);

    if(result.pass)
    {
        result.message = 'Expected '+ identifier + ' not to have been called with '+JSON.stringify(expected)+', called with '+JSON.stringify(actual)+', missing matches for '+JSON.stringify(mismatchedExpects);
    }
    else
    {
        result.message = 'Expected '+ identifier + ' to have been called with at least '+JSON.stringify(expected)+', called with '+JSON.stringify(actual)+', missing matches for '+JSON.stringify(mismatchedExpects);
    }

    return result;
};

var customMatchers = {

    toBeArray: function (util, customEqualityTesters) {
        return {
            compare: function (actual, expected) {
                if (expected === undefined) {
                    expected = '';
                }
                var result = {};
                result.pass = actual instanceof Array;
                if (result.pass)
                {
                    result.message = "Expected " + actual + " not to be array";
                }
                else
                {
                    result.message = "Expected " + actual + " to be array";
                }
                return result;
            }
        };
    },
    toBeNumber: function (util, customEqualityTesters)
    {
        return {
            compare: function (actual)
            {
                var result = {};
                result.pass = Object.prototype.toString.call(actual) == '[object Number]';
                if(result.pass)
                {
                    result.message = "Expected " + actual + " not to be number";
                }
                else
                {
                    result.message = "Expected " + actual + " to be number";
                }
                return result;
            }
        };
    },

    toBeObject: function (util, customEqualityTesters)
    {
        return {
            compare: function (actual)
            {
                var result = {};
                result.pass = Object.prototype.toString.call(actual) == '[object Object]';
                if(result.pass)
                {
                    result.message = "Expected " + actual + " not to be plain object";
                }
                else
                {
                    result.message = "Expected " + actual + " to be plain object";
                }
                return result;
            }
        };
    },

    toBeString: function (util, customEqualityTesters)
    {
        return {
            compare: function (actual)
            {
                var result = {};
                result.pass = Object.prototype.toString.call(actual) == '[object String]';
                if(result.pass)
                {
                    result.message = "Expected " + actual + " not to be string";
                }
                else
                {
                    result.message = "Expected " + actual + " to be string";
                }
                return result;
            }
        };
    },
    toBeInstanceOf: function (util, customEqualityTesters) {
        return {
            compare: function (actual, expected) {
                var result = {};
                result.pass = actual instanceof expected;
                if (result.pass) {
                    result.message = "Expected " + actual + " not to be instance of " + expected;
                } else {
                    result.message = "Expected " + actual + " to be instance of " + expected;
                }
                return result;
            }
        };
    },

    toBeInRange: function (util, customEqualityTesters) {
        return {
            compare: function (actual, a, b) {
                var result = {};
                result.pass = actual <= b && actual >= a;
                if (result.pass) {
                    result.message = "Expected " + actual + " not to be in range " + a + " - " + b;
                } else {
                    result.message = "Expected " + actual + " to be in range " + a + " - " + b;
                }
                return result;
            }
        };
    },
    toBePresent: function (util, customEqualityTesters) {
        return {
            compare: function (actual) {
                var result = {};
                if((actual instanceof jQuery) === false)
                {
                   result.pass = false;
                   result.message = 'Expected '+actual+' to be a jQuery object';
                }
                else
                {
                    if(jQuery.contains(document, actual[0]))
                    {
                        result.pass = actual.is(':visible');
                    }
                    else
                    {
                        result.pass = actual.length === 1 && actual.css('display') !== 'none';
                    }
                    if (result.pass) {
                        result.message = "Expected " + actual.selector + " not to be present";
                    } else {
                        result.message = "Expected " + actual.selector + " to be present";
                    }
                }
                return result;
            }
        };
    },
    toBeHidden: function (util, customEqualityTesters) {
        return {
            compare: function (actual) {
                var result = {};
                if((actual instanceof jQuery) === false)
                {
                   result.pass = false;
                   result.message = 'Expected '+actual+' to be a jQuery object';
                }
                else
                {
                    if(jQuery.contains(document, actual[0]))
                    {
                        result.pass = actual.is(':hidden');
                    }
                    else
                    {
                        result.pass = actual.length === 1 && actual.css('display') === 'none';
                    }
                    if (result.pass) {
                        result.message = "Expected " + actual.selector + " not to be hidden";
                    } else {
                        result.message = "Expected " + actual.selector + " to be hidden";
                    }
                }
                return result;
            }
        };
    },
    toContainAtleast: function ()
    {
        return {
            compare: function (actual, expected) {
                return objectContains(expected, actual, "object");
            }
        };
    },
    toHaveBeenCalledWithAtleast: function () {
        return {
            compare: function() {
                var args = Array.prototype.slice.call(arguments, 0);
                var actual = args[0];
                var expectedOptions = args.slice(1).pop();
                var result = {pass: false};
                var identity = _.isFunction(actual.and.identity) ? actual.and.identity() : actual.and.identity;

                if (arguments.length > 2) {
                    throw new Error('toHaveBeenCalledWithAtleast does not take more than 1 argument');
                }

                if (!jasmine.isSpy(actual)) {
                    throw new Error('Expected a spy, but got ' + jasmine.pp(actual) + '.');
                }

                if (!actual.calls.any()) {
                    result.message = "Expected spy " + identity + " to have been called with " + jasmine.pp(expectedOptions) + " but it was never called.";
                    return result;
                }

                var calledArgs = actual.calls.mostRecent().args[0] || {};
                return objectContains(expectedOptions, calledArgs, "spy " + identity);
            }
        };
    },
    toContainJQueryElement: function (util, customEqualityTesters) {
        return {
            compare: function (haystack, needle) {
                var result = {};
                if((haystack instanceof jQuery) == false)
                {
                    result.pass = false;
                    result.message = 'Expected '+haystack+' to be a jQuery object';
                }
                else if((needle instanceof jQuery) == false)
                {
                    result.pass = false;
                    result.message = 'Expected '+needle+' to be a jQuery object';
                }
                else
                {
                    result.pass = jQuery.contains(haystack.get(0), needle.get(0));

                    var getTextForJQueryElement = function(parentElement)
                    {
                        var text = 'null';
                        if(needle.get(0))
                        {
                            var element = parentElement.get(0);
                            text = element.nodeName.toLowerCase();
                            if(element.id)
                            {
                                text += "#"+element.id;
                            }
                            if(element.className)
                            {
                                text += "."+element.className.replace(/\s+/g, ".");
                            }
                        }
                        return text;
                    };

                    var haystackText = getTextForJQueryElement(haystack);
                    var needleText = getTextForJQueryElement(needle);

                    if (result.pass) {
                        result.message = "Expected " + needleText + " not to be found within "+haystackText;
                    } else {
                        result.message = "Expected " + needleText + " to be found within "+haystackText;
                    }
                }
                return result;
            }
        };
    },
    toBeJQueryElement: function (util, customEqualityTesters) {
        return {
            compare: function (expected, actual) {
                var result = {};
                if((expected instanceof jQuery) == false)
                {
                    result.pass = false;
                    result.message = 'Expected '+expected+' to be a jQuery object';
                }
                else if((actual instanceof jQuery) == false)
                {
                    result.pass = false;
                    result.message = 'Expected '+actual+' to be a jQuery object';
                }
                else
                {
                    result.pass = expected.is(actual);

                    var expectedText = getTextForJQueryElement(expected);
                    var actualText = getTextForJQueryElement(actual);

                    if (result.pass) {
                        result.message = "Expected " + actualText + " not to be " + expectedText;
                    } else {
                        result.message = "Expected " + actualText + " to be " + expectedText;
                    }
                }
                return result;
            }
        };
    },
    toHaveBeenCalledXTimes: function (util, customEqualityTesters) {
        return {
            compare: function (spy, expectedCount) {
                var result = {};

                var actualCount = spy.calls.count();
                result.pass = actualCount == expectedCount;

                if (result.pass) {
                    result.message = "Expected spy not to have been called " + expectedCount + " times";
                } else {
                    result.message = "Expected spy to have been called " + expectedCount + " times, was called  " + actualCount + " times";
                }
                return result;
            }
        };
    },
    toBeAJQueryPromise: function (util, customEqualityTesters)
    {
        return {
            compare: function (actual, oneOf)
            {
                var result = {
                    pass: true
                };
                if(!_.isObject(actual) || !_.isFunction(actual['then']) || !_.isFunction(actual['fail']) || !_.isFunction(actual['done']) || !_.isFunction(actual['always']) || !_.isFunction(actual['progress']))
                {
                    result.pass = false;
                }
                if(result.pass)
                {
                    result.message = "Expected " + actual + " not to be a JQuery Promise";
                }
                else
                {
                    result.message = "Expected " + actual + " to be a JQuery Promise";
                }
                return result;
            }
        }
    },
    toHaveOwnProperties: function (util, customEqualityTesters)
    {
        return {
            compare: function (actual)
            {
                var result = {};
                var args = [].splice.call(arguments, 0);

                //remove first argument
                args.splice(0, 1);

                var missing = [];
                result.pass = true;
                args.forEach(function (propertyName)
                {
                    if (!(propertyName in actual))
                    {
                        result.pass = false;
                        missing.push(propertyName);
                    }
                });

                if (!result.pass)
                {
                    result.message = "Expected object to contain " + missing.join(', ') + ' but they contained ' + Object.keys(actual).join(', ');
                }
                return result;
            }
        }
    },

    toBeOneOf: function (util, customEqualityTesters)
    {
        return {
            compare: function (actual, oneOf)
            {
                var result = {};
                var match = 0;

                result.pass = true;
                oneOf.forEach(function (oneOfString)
                {
                    if (oneOfString == actual)
                    {
                        match++;
                    }
                });

                if(!match)
                {
                    result.pass = false;
                    result.message = "Expected object to contain " + missing.join(', ') + ' but they contained ' + Object.keys(actual).join(', ');
                }

                return result;
            }
        }
    }
};

beforeEach(function()
{
    jasmine.addMatchers(customMatchers);
});
